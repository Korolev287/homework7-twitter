const spiner = document.querySelector('.lds-circle')
spiner.classList.add('spiner-display-none');

class Card {
  constructor(postId ,title, body, name, email) {
    this.postId = postId
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
  }
  render() {
    const div = document.createElement('div')
    div.classList.add('card');
    const cardTitle = document.createElement('h2')
    cardTitle.innerText = this.title
    const cardBody = document.createElement('p')
    cardBody.innerText = this.body;
    const clientName = document.createElement('h5')
    clientName.innerText = this.name;
    const email = document.createElement('span')
    email.innerText = this.email;
    const button = document.createElement('button')
    button.innerText = 'Удалить'
    button.addEventListener('click', async (e) => {
      const resp = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}` , {
        method: 'DELETE'
      })
      if(resp.ok){
        div.remove()
      }
    })
    document.body.append(div)
    div.append(cardTitle, cardBody, clientName, email, button)
  }
}

const fetchFunc = async () => {
  try {
    spiner.classList.remove('spiner-display-none');
    const usersResp = await fetch('https:ajax.test-danit.com/api/json/users');
    const users = await usersResp.json();
    const postsResp = await fetch('https://ajax.test-danit.com/api/json/posts');
    const posts = await postsResp.json();
    spiner.classList.add('spiner-display-none');
    dataResiver(users, posts)

  } catch (error) {
    spiner.classList.add('spiner-display-none');
    console.log(error);
  }
}

const dataResiver = (users, posts) => {
  const usersNeadetProperties = users.map((el) => { 
    const {id, name, email} = el;
    return {id, name, email}
   })
  const postsCardData = posts.map((el) => {
    const user = usersNeadetProperties.find(({ id }) => {
      return id === el.userId;
    })
    return { ...user, ...el  }
  })
  
 postsCardData.forEach((el)=>{
    const{body, email, id, name, title } = el;
    const card = new Card(id, title, body, name, email) 
    card.render()
 })



}

fetchFunc()

